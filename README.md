# mcompletescript

![](https://img.shields.io/badge/written%20in-Lua-blue)

A PtokaX extension adding several features.

User features:
- Catchup chat history
- Full chat archive
- User registration and password change support
- Block search terms
- Block invisible space characters in user nicks
- Group chat support with UserCommand integration

Administrator features:
- Manage ptokax scripts
- Retrieve hub statistics
- Impersonate users

Tags: nmdc


## Download

- [⬇️ mCompleteScript_v20.lua](dist-archive/mCompleteScript_v20.lua) *(21.02 KiB)*
